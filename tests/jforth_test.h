#ifndef _HOME_JONO_GIT_JFORTH_TESTS_JFORTH_TEST_H
#define _HOME_JONO_GIT_JFORTH_TESTS_JFORTH_TEST_H

#include <stdio.h>

static int num_tests = 0;
static int num_failures = 0;

#define TEST_SUCCESS (num_tests > 0 && num_failures == 0 ? 0 : 1)

#define TEST_SUMMARY                                                           \
  printf("%d test completed; %d failures\n", num_tests, num_failures)

#define EXPECT_EQ_LINE(a, b, line)                                             \
  {                                                                            \
    ++num_tests;                                                               \
    if ((a) != (b)) {                                                          \
      ++num_failures;                                                          \
      printf("test %d, line %d failed: expected %s == %s\n", num_tests, line,  \
             #a, #b);                                                          \
    }                                                                          \
  }

#define EXPECT_EQ(a, b) EXPECT_EQ_LINE(a, b, __LINE__)

#define EXPECT_NEQ_LINE(a, b, line)                                            \
  {                                                                            \
    ++num_tests;                                                               \
    if ((a) == (b)) {                                                          \
      ++num_failures;                                                          \
      printf("test %d, line %d failed: expected %s != %s\n", num_tests, line,  \
             #a, #b);                                                          \
    }                                                                          \
  }

#define EXPECT_NEQ(a, b) EXPECT_NEQ_LINE(a, b, __LINE__)

#define EXPECT_WORD_LINE(name, line)                                           \
  EXPECT_NEQ_LINE(NULL, vm_find_word(vm.dictionary, name), line)

#define EXPECT_WORD(name) EXPECT_WORD_LINE(name, __LINE__)

#define EXPECT_MACRO_LINE(name, line)                                          \
  EXPECT_NEQ_LINE(NULL, vm_find_word(vm.macros, name), line)

#define EXPECT_MACRO(name) EXPECT_MACRO_LINE(name, __LINE__)

#endif
