#include "jforth/vm.h"

#include "jforth_test.h"

int main() {
  vm_t vm;
  vm_initialize(&vm, stdin);

  EXPECT_EQ(vm.sp, 0);

  /* check core word needed to bootstrap interpreter */
  EXPECT_WORD("_get_token");

  EXPECT_WORD("dup");
  EXPECT_WORD("0branch");
  EXPECT_WORD("branch");
  EXPECT_WORD("_interpret");
  EXPECT_WORD("drop");
  EXPECT_WORD("bye");

  /* core built-ins needed for basic operations */
  EXPECT_WORD("create");
  EXPECT_WORD(",");
  EXPECT_WORD("@");
  EXPECT_WORD("!");
  EXPECT_WORD("ret");
  EXPECT_WORD("cells");
  EXPECT_WORD(":");
  EXPECT_WORD("'");
  EXPECT_WORD("execute");

  EXPECT_MACRO(";");
  EXPECT_MACRO("if");
  EXPECT_MACRO("else");
  EXPECT_MACRO("then");
  EXPECT_MACRO("do");
  EXPECT_MACRO("loop");

  TEST_SUMMARY;
  return TEST_SUCCESS;
}
