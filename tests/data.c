#include "jforth/vm.h"

#include "jforth_test.h"

int main() {
  vm_t vm;
  vm_initialize(&vm, stdin);

  EXPECT_EQ(vm.sp, 0);

  /* core operations on data segment */
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, "create"));
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, ","));
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, "!"));
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, "@"));
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, "cells"));
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, "_literal"));

  TEST_SUMMARY;
  return TEST_SUCCESS;
}
