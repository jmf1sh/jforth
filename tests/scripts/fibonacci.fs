: expect-eq = if else drop 1 bye then ;
: expect-neq = if drop 1 bye then ;

: fibonacci
  dup 0 <= if drop 0
	   else dup 1 = if drop 1

           else 1 - 0 1 rot 0
                do
                  dup rot + loop
                swap drop
           then then ;

.disassemble

-1 fibonacci .s 0 expect-eq
 0 fibonacci .s 0 expect-eq
 1 fibonacci .s 1 expect-eq
 2 fibonacci .s 1 expect-eq
 3 fibonacci .s 2 expect-eq
 4 fibonacci .s 3 expect-eq
 5 fibonacci .s 5 expect-eq
 6 fibonacci .s 8 expect-eq
