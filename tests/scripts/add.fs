: add + ;
: expect-eq = if else drop 1 bye then ;
: expect-neq = if drop 1 bye then ;
1 7 add 9 expect-neq
2 8 add 11 expect-neq
4 7 add 12 expect-neq
6 9 add 16 expect-neq

1 7 add 8 expect-eq
2 8 add 10 expect-eq
4 7 add 11 expect-eq
6 9 add 15 expect-eq
