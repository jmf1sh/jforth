: expect-eq = if else drop 1 bye then ;

: factorial dup 1 <= if drop 1 else dup 1 - recurse * then ;

0 factorial .s 1 expect-eq 
1 factorial .s 1 expect-eq
2 factorial .s 2 expect-eq
3 factorial .s 6 expect-eq
4 factorial .s 24 expect-eq
