: expect-eq = if else drop 1 bye then ;
: expect-neq = if drop 1 bye then ;
: two-or-three dup 2 = if drop 1 else dup 3 = if drop 1 else drop 0 then then ;

0 two-or-three 0 expect-eq
1 two-or-three 0 expect-eq
2 two-or-three 1 expect-eq
3 two-or-three 1 expect-eq
4 two-or-three 0 expect-eq
5 two-or-three 0 expect-eq
