: expect-eq = if else drop 1 bye then ;
: expect-neq = if drop 1 bye then ;

1 7 + 9 expect-neq
2 8 + 11 expect-neq
4 7 + 12 expect-neq
6 9 + 16 expect-neq

1 7 + 8 expect-eq
2 8 + 10 expect-eq
4 7 + 11 expect-eq
6 9 + 15 expect-eq

1 7 - -6 expect-eq
2 8 - -6 expect-eq
4 7 - -3 expect-eq
6 9 - -3 expect-eq

7 1 - 6 expect-eq
8 2 - 6 expect-eq
7 4 - 3 expect-eq
9 6 - 3 expect-eq


7 3 / 2 expect-eq
9 2 / 4 expect-eq
-4 -3 / 1 expect-eq

4 4 = 0 expect-neq
3 4 = 0 expect-eq
3 4 < 0 expect-neq
3 4 > 0 expect-eq

4 4 > 0 expect-eq
4 4 >= 0 expect-neq

4 4 < 0 expect-eq
4 4 <= 0 expect-neq


0x0f 0xf0 or 0xff expect-eq
0x0f 0xf0 and 0x00 expect-eq
