: expect-eq = if else drop 1 bye then ;
: expect-neq = if drop 1 bye then ;

depth 0 .s expect-eq
1 2 3 depth 3 .s expect-eq drop drop drop

2 2 = 0 .s expect-neq
3 dup = 0 .s expect-neq
3 dup + 6 .s expect-eq


