#include "jforth/vm.h"

#include "jforth_test.h"

int main() {
  vm_t vm;
  vm_initialize(&vm, stdin);

  EXPECT_EQ(vm.sp, 0);

  /* check that core branch operations are registered */
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, "branch"));
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, "0branch"));
  EXPECT_NEQ(NULL, vm_find_word(vm.macros, "if"));
  EXPECT_NEQ(NULL, vm_find_word(vm.macros, "else"));
  EXPECT_NEQ(NULL, vm_find_word(vm.macros, "then"));
  EXPECT_NEQ(NULL, vm_find_word(vm.macros, "do"));
  EXPECT_NEQ(NULL, vm_find_word(vm.macros, "loop"));

  /* words used internally */
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, "1+"));
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, "2dup"));
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, ">"));
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, ">r"));
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, "r>"));
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, "drop"));
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, "ret"));
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, "_literal"));

  TEST_SUMMARY;
  return TEST_SUCCESS;
}
