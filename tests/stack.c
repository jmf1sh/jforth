#include "jforth/vm.h"

#include "jforth_test.h"

int main() {
  vm_t vm;
  vm_initialize(&vm, stdin);

  EXPECT_EQ(vm.sp, 0);

  /* check that core stack operations are registered */
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, "dup"));
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, "drop"));
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, ".s"));
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, "2dup"));
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, "swap"));
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, "rot"));
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, "depth"));
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, ">r"));
  EXPECT_NEQ(NULL, vm_find_word(vm.dictionary, "r>"));

  TEST_SUMMARY;
  return TEST_SUCCESS;
}
