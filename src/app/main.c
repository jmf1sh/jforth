#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "jforth/vm.h"

int main(int argc, char *argv[]) {
  vm_t vm;
  FILE *input_file = NULL;

  if (argc == 1) {
    input_file = stdin;
    printf("welcome to jforth. type 'bye' to quit\n");
  } else if (argc == 2) {
    input_file = fopen(argv[1], "r");
    if (input_file == NULL) {
      printf("ERROR: could not open file '%s'\n", argv[1]);
    }
  } else {
    printf("Usage: jfrepl [script]\n");
    return 1;
  }

  vm_initialize(&vm, input_file);

  while (!vm.quit) {
    vm.current_word = (vm.ip++)->as_word;
    vm.current_word->exec(&vm);
  }

  return vm.data_stack[vm.sp].as_int;
}
