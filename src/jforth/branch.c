#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vm.h"

static const vm_cell_t NULL_CELL = {.as_code = NULL};

VM_BUILTIN(0branch) {
  vm_int cond = vm_pop(vm).as_int;
  vm_cell_t *dest = (vm->ip++)->as_code;
  if (cond == 0) {
    vm->ip = dest;
  }
}

VM_BUILTIN(branch) { vm->ip = vm->ip->as_code; }

VM_BUILTIN(ret) { vm->ip = vm_ret_pop(vm).as_code; }

VM_BUILTIN(if) {
  VM_COMPILE_WORD_BY_NAME("0branch");
  vm_cell_t val;
  val.as_code = vm->code_ptr++;
  vm_push(vm, val);
}

VM_BUILTIN(else) {
  vm_cell_t *branch_dest = vm_pop(vm).as_code;
  VM_COMPILE_WORD_BY_NAME("branch");
  vm_cell_t val;
  val.as_code = vm->code_ptr++;
  branch_dest->as_code = vm->code_ptr;
  vm_push(vm, val);
}

VM_BUILTIN(then) {
  vm_cell_t *branch_dest = vm_pop(vm).as_code;
  branch_dest->as_code = vm->code_ptr;
}

/**
 * ( limit start -- )
 */
VM_BUILTIN(do) {
  // return stack now contains ( limit count )
  vm_cell_t do_start;
  do_start.as_code = vm->code_ptr;
  VM_COMPILE_WORD_BY_NAME("2dup");    // limit count limit count
  VM_COMPILE_WORD_BY_NAME(">");       // limit count condition
  VM_COMPILE_WORD_BY_NAME("0branch"); // limit count
  vm_cell_t do_end;
  do_end.as_code = vm->code_ptr++;
  VM_COMPILE_WORD_BY_NAME(">r");
  VM_COMPILE_WORD_BY_NAME(">r");
  // return stack contains count limit
  vm_push(vm, do_start);
  vm_push(vm, do_end);
}

VM_BUILTIN(loop) {
  vm_cell_t *do_end = vm_pop(vm).as_code;
  vm_cell_t *do_start = vm_pop(vm).as_code;

  VM_COMPILE_WORD_BY_NAME("r>");
  VM_COMPILE_WORD_BY_NAME("r>");
  // stack now contains (limit count)
  VM_COMPILE_WORD_BY_NAME("1+");
  VM_COMPILE_WORD_BY_NAME("branch");
  (vm->code_ptr++)->as_code = do_start;
  do_end->as_code = vm->code_ptr;
  VM_COMPILE_WORD_BY_NAME("drop");
  VM_COMPILE_WORD_BY_NAME("drop");
}

VM_BUILTIN(recurse) { VM_COMPILE_WORD(vm->current_compile_word); }

void vm_add_branch_builtins(vm_t *vm) {
  VM_ADD_BUILTIN("0branch", 0branch);
  VM_ADD_BUILTIN("branch", branch);
  VM_ADD_BUILTIN("ret", ret);

  VM_ADD_MACRO("if", if);
  VM_ADD_MACRO("else", else);
  VM_ADD_MACRO("then", then);

  vm_add_word(vm, &vm->macros, "do", &vm_builtin_do, NULL_CELL);
  vm_add_word(vm, &vm->macros, "loop", &vm_builtin_loop, NULL_CELL);
  vm_add_word(vm, &vm->macros, "recurse", &vm_builtin_recurse, NULL_CELL);
}
