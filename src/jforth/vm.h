#ifndef _HOME_JONO_GIT_JFORTH_SRC_JFORTH_VM_H
#define _HOME_JONO_GIT_JFORTH_SRC_JFORTH_VM_H

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define VM_CHECK_BOUNDS 1

#ifndef VM_MEMORY_SIZE
#define VM_MEMORY_SIZE 0x2000
#endif

#ifndef VM_STACK_SIZE
#define VM_STACK_SIZE 0x100
#endif

#ifndef VM_RETURN_STACK_SIZE
#define VM_RETURN_STACK_SIZE 0x100
#endif

#ifndef VM_LINEBUF_SIZE
#define VM_LINEBUF_SIZE 0x100
#endif

#define VM_REALLOC(type, ptr, size) (type *)realloc(ptr, (size) * sizeof(type))

#define VM_COMPILE_WORD(value) (vm->code_ptr++)->as_word = (value)

#define VM_COMPILE_WORD_BY_NAME(name)                                          \
  (vm->code_ptr++)->as_word = vm_find_word(vm->dictionary, name)

#define VM_BUILTIN(func) void vm_builtin_##func(vm_t *vm)

#define VM_ADD_BUILTIN(name, func)                                             \
  vm_add_word(vm, &vm->dictionary, name, &vm_builtin_##func, NULL_CELL)

#define VM_ADD_MACRO(name, func)                                               \
  vm_add_word(vm, &vm->macros, name, &vm_builtin_##func, NULL_CELL)

typedef ptrdiff_t vm_int;

struct vm;
struct vm_word;
union vm_cell;
typedef void (*vm_word_fn)(struct vm *);

typedef struct vm {
  int quit;
  int compile;
  struct vm_word *dictionary;
  struct vm_word *macros;

  char *linebuf;

  union vm_cell *data_stack;
  /* union vm_cell *data_segment; */
  /* vm_int dp; // data pointer */

  vm_int sp;

  union vm_cell *code_segment;
  union vm_cell *code_ptr;
  union vm_cell *ip;
  vm_int code_size;

  struct vm_word *current_word;
  struct vm_word *current_compile_word;

  union vm_cell *return_stack;
  vm_int rp;

  FILE *input_file;
} vm_t;

typedef union vm_cell {
  vm_int as_int;
  char *as_str;
  float as_float;
  struct vm_word *as_word;
  union vm_cell *as_code;
} vm_cell_t;

typedef struct vm_word {
  char *name;
  void (*exec)(vm_t *);
  struct vm_word *next;
  /* data must be last member for create/allot */
  vm_cell_t data;
} vm_word_t;

vm_word_t *vm_find_word(vm_word_t *definitions, const char *name);

void vm_grow_data(vm_t *vm);

void vm_grow_code(vm_t *vm);

void vm_initialize(vm_t *vm, FILE *input_file);

char *vm_next_token(vm_t *vm);

/**
 * Create a new word in the VM, but do not add it to the dictionary.
 * Normally the function vm_add_word should be called instead, except for
 * a few built-ins that are required for the interpreter.
 */
vm_word_t *vm_create_word(vm_t *vm, const char *name, vm_word_fn exec,
                          vm_cell_t data);

/**
 * Create a word and add it to the list of definitions.
 */
vm_word_t *vm_add_word(vm_t *vm, vm_word_t **definitions, const char *name,
                       vm_word_fn exec, vm_cell_t data);

void vm_push(vm_t *vm, vm_cell_t val);

void vm_ret_push(vm_t *vm, vm_cell_t val);

vm_cell_t vm_pop(vm_t *vm);

vm_cell_t vm_ret_pop(vm_t *vm);

void vm_builtin_bye(vm_t *vm);

void vm_builtin_drop(vm_t *vm);

void vm_builtin_dup(vm_t *vm);

void vm_builtin_dots(vm_t *vm);

void vm_builtin__get_token(vm_t *vm);

void vm_builtin__interpret(vm_t *vm);

void vm_builtin_0branch(vm_t *vm);

void vm_builtin_branch(vm_t *vm);

void vm_builtin_call(vm_t *vm);

void vm_builtin_return(vm_t *vm);

void vm_builtin_colon(vm_t *vm);

void vm_builtin_semicolon(vm_t *vm);

void vm_builtin_if(vm_t *vm);

void vm_builtin_else(vm_t *vm);

void vm_builtin_then(vm_t *vm);

void vm_builtin_eq(vm_t *vm);

void vm_builtin_neq(vm_t *vm);

void vm_builtin_literal(vm_t *vm);

/* functions below for bootstrapping the interpreter */
void vm_add_stack_builtins(vm_t *vm);
void vm_add_branch_builtins(vm_t *vm);
void vm_add_arithmetic_builtins(vm_t *vm);
void vm_add_parser_builtins(vm_t *vm);
void vm_add_data_builtins(vm_t *vm);

vm_cell_t *vm_alloc(vm_t *vm, vm_int size);

#endif
