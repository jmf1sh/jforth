#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vm.h"

static const vm_cell_t NULL_CELL = {.as_code = NULL};

vm_word_t *vm_find_word(vm_word_t *definitions, const char *name) {
  vm_word_t *word = definitions;

  while (word) {
    if (strcmp(word->name, name) == 0) {
      return word;
    }
    word = word->next;
  }
  return 0;
}

/**
 * Create a word and add it to the list of definitions.
 */
vm_word_t *vm_add_word(vm_t *vm, vm_word_t **definitions, const char *name,
                       vm_word_fn exec, vm_cell_t data) {
  vm_word_t *new_word = vm_create_word(vm, name, exec, data);
  new_word->next = *definitions;
  *definitions = new_word;
  return new_word;
}

VM_BUILTIN(bye) { vm->quit = 1; }

VM_BUILTIN(interpret) {
  char *tok = vm_pop(vm).as_str;
  if (tok == 0) {
    puts("FATAL: tried to interpret past end of input stream\n");
    vm->quit = 1;
    return;
  }

  // try to parse literal
  vm_cell_t lit_val;
  // double val_f;
  char *endptr = NULL;

  // is it a macro?
  vm->current_word = vm_find_word(vm->macros, tok);
  if (vm->current_word != NULL) {
    if (!vm->compile) {
      printf("ERROR: '%s' is a compile-only word\n", tok);
      return;
    }

    vm->current_word->exec(vm);
    return;
  }

  // is it a word in the dictionary?
  vm->current_word = vm_find_word(vm->dictionary, tok);
  if (vm->current_word != NULL) {
    // handle...
    if (vm->compile) {
      vm->code_ptr->as_word = vm->current_word;
      ++vm->code_ptr;
    } else {
      // interpreting, execute immediately
      vm->current_word->exec(vm);
    }
    return;
  }

  // otherwise, it may be a literal...
  // try to parse literal int
  lit_val.as_int = strtol(tok, &endptr, 0);
  if (*endptr == 0) {

    if (vm->compile) {
      vm_word_t *xt_literal = vm_find_word(vm->dictionary, "_literal");
      VM_COMPILE_WORD(xt_literal);
      (vm->code_ptr++)->as_int = lit_val.as_int;
    } else {
      vm_push(vm, lit_val);
    }
    return;
  }

  // at this point, it unidentified
  printf("> unrecognized word '%s'\n", tok);
}

VM_BUILTIN(call) {
  vm_cell_t val;
  val.as_code = vm->ip;
  vm_ret_push(vm, val);
  vm->ip = vm->current_word->data.as_code;
}

VM_BUILTIN(colon) {
  vm->compile = 1;
  char *name = vm_next_token(vm);
  // vm_cell_t data = {.as_code = vm->code_ptr};
  vm_word_t *new_word =
      vm_add_word(vm, &vm->dictionary, name, vm_builtin_call, NULL_CELL);
  vm->current_compile_word = new_word;
  new_word->data.as_code = vm->code_ptr;
}

VM_BUILTIN(semicolon) {
  VM_COMPILE_WORD_BY_NAME("ret");
  vm->compile = 0;
  vm->current_compile_word = NULL;
}

VM_BUILTIN(apostrophe) {
  char *name = vm_next_token(vm);
  vm_word_t *word = vm_find_word(vm->dictionary, name);
  vm_cell_t val;
  val.as_word = word;
  vm_push(vm, val);
}

VM_BUILTIN(execute) {
  vm_word_t *word = vm_pop(vm).as_word;
  vm_cell_t val;
  val.as_code = vm->ip;
  vm_ret_push(vm, val);
  vm->ip = word->data.as_code;
}

VM_BUILTIN(disassemble) {
  vm_cell_t *start = vm->code_segment;
  vm_cell_t *end = vm->code_ptr;

  for (vm_cell_t *current = start; current < end; ++current) {
    // find word?
    vm_word_t *word = vm->dictionary;

    while (word != NULL) {
      if (current == word->data.as_code) {
        printf("%s:\n", word->name);
      }

      if (current->as_word == word) {
        printf("    %p    %s\n", (void *)current, word->name);
        break;
      }
      word = word->next;
    }

    // didn't find word, so literal?
    if (word == NULL) {
      printf("    %p    %p\n", (void *)current, (void *)current->as_int);
    }
  }
}

void vm_initialize(vm_t *vm, FILE *input_file) {
  vm->input_file = input_file;
  vm->quit = 0;
  vm->dictionary = 0;
  vm->macros = 0;
  vm->compile = 0;
  vm->current_word = NULL;
  vm->current_compile_word = NULL;

  vm->code_segment = malloc(VM_MEMORY_SIZE);

  if (vm->code_segment == NULL) {
    printf("FATAL: could not allocate VM memory\n");
    exit(1);
  }

  vm->code_ptr = vm->code_segment;
  vm->code_size = 0;
  vm->ip = 0;

  vm->data_stack = vm_alloc(vm, VM_STACK_SIZE / sizeof(vm_cell_t));
  vm->sp = 0;
  // initial 0 on stack
  vm->data_stack[vm->sp].as_int = 0;

  vm->return_stack = vm_alloc(vm, VM_RETURN_STACK_SIZE / sizeof(vm_cell_t));
  vm->rp = 0;
  // initial 0 on stack
  vm->return_stack[vm->rp].as_int = 0;

  // for reading tokens
  vm->linebuf = (char *)vm_alloc(vm, VM_LINEBUF_SIZE / sizeof(vm_cell_t));

  // bootstrap interpreter
  VM_ADD_BUILTIN("_interpret", interpret);
  VM_ADD_BUILTIN("bye", bye);
  VM_ADD_BUILTIN(".disassemble", disassemble);
  VM_ADD_BUILTIN(":", colon);
  VM_ADD_BUILTIN("'", apostrophe);
  VM_ADD_BUILTIN("execute", execute);
  VM_ADD_MACRO(";", semicolon);


  vm_add_stack_builtins(vm);
  vm_add_branch_builtins(vm);
  vm_add_data_builtins(vm);
  vm_add_parser_builtins(vm);
  vm_add_arithmetic_builtins(vm);

  // build interpreter
  vm_cell_t *begin = vm->code_ptr;
  VM_COMPILE_WORD_BY_NAME("_get_token");
  VM_COMPILE_WORD_BY_NAME("dup");
  VM_COMPILE_WORD_BY_NAME("0branch");
  vm_cell_t *done = vm->code_ptr++;
  VM_COMPILE_WORD_BY_NAME("_interpret");
  VM_COMPILE_WORD_BY_NAME("branch");
  (vm->code_ptr++)->as_code = begin;
  done->as_code = vm->code_ptr;
  VM_COMPILE_WORD_BY_NAME("drop");
  VM_COMPILE_WORD_BY_NAME("bye");
  vm->ip = begin;

  /* give a name to the REPL loop */
  vm_word_t *repl = VM_ADD_BUILTIN("_repl", call);
  repl->data.as_code = begin;
}
