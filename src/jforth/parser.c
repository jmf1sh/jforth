#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vm.h"

static const vm_cell_t NULL_CELL = {.as_code = NULL};

char *vm_next_token(vm_t *vm) {
  vm_int pos = 0;

  while (pos < VM_LINEBUF_SIZE) {
    int c = fgetc(vm->input_file);

    if (c == ' ' || c == '\t' || c == '\n' || c == EOF) {
      if (pos > 0) {
        vm->linebuf[pos] = 0;
        return vm->linebuf;
      }
      if (c == EOF) {
        return 0;
      }

      continue;
    }
    vm->linebuf[pos++] = (char)c;
  }
  puts("ERROR: line exceeded maximum size!\n");
  return 0;
}

VM_BUILTIN(get_token) {
  char *token = vm_next_token(vm);
  vm_cell_t val;
  val.as_str = token;
  vm_push(vm, val);
}

void vm_add_parser_builtins(vm_t *vm) {
  VM_ADD_BUILTIN("_get_token", get_token);
}
