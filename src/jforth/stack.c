#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vm.h"

static const vm_cell_t NULL_CELL = {.as_code = NULL};

VM_BUILTIN(to_r) { vm_ret_push(vm, vm_pop(vm)); }

VM_BUILTIN(from_r) { vm_push(vm, vm_ret_pop(vm)); }

void vm_push(vm_t *vm, vm_cell_t val) {
  if (vm->sp == VM_STACK_SIZE) {
    printf("FATAL: stack overflow\n");
    exit(1);
  }
  vm->data_stack[++vm->sp] = val;
}

void vm_ret_push(vm_t *vm, vm_cell_t val) { vm->return_stack[++vm->rp] = val; }

vm_cell_t vm_pop(vm_t *vm) {
  vm_cell_t val = vm->data_stack[vm->sp];
  if (vm->sp != 0) {
    --vm->sp;
    if (vm->sp < 0) {
      printf("FATAL: stack underflow\n");
      exit(1);
    }
  }
  return val;
}

vm_cell_t vm_ret_pop(vm_t *vm) {
  vm_cell_t val = vm->return_stack[vm->rp];
  --vm->rp;
  return val;
}

VM_BUILTIN(swap) {
  vm_cell_t b = vm_pop(vm);
  vm_cell_t a = vm_pop(vm);
  vm_push(vm, b);
  vm_push(vm, a);
}

VM_BUILTIN(rot) {
  vm_cell_t c = vm_pop(vm);
  vm_cell_t b = vm_pop(vm);
  vm_cell_t a = vm_pop(vm);
  vm_push(vm, b);
  vm_push(vm, c);
  vm_push(vm, a);
}

VM_BUILTIN(depth) {
  vm_cell_t val;
  val.as_int = vm->sp;
  vm_push(vm, val);
}

VM_BUILTIN(drop) { vm_pop(vm); }

VM_BUILTIN(dup) {
  vm_cell_t val = vm_pop(vm);
  vm_push(vm, val);
  vm_push(vm, val);
}

VM_BUILTIN(2dup) {
  vm_cell_t b = vm_pop(vm);
  vm_cell_t a = vm_pop(vm);
  vm_push(vm, a);
  vm_push(vm, b);
  vm_push(vm, a);
  vm_push(vm, b);
}

VM_BUILTIN(dots) {
  for (int i = 0; i <= vm->sp; ++i) {
    printf("%ld ", vm->data_stack[i].as_int);
  }
  puts("\n");
}

void vm_add_stack_builtins(vm_t *vm) {

  VM_ADD_BUILTIN("dup", dup);
  VM_ADD_BUILTIN("drop", drop);
  VM_ADD_BUILTIN(".s", dots);
  VM_ADD_BUILTIN("2dup", 2dup);
  VM_ADD_BUILTIN("swap", swap);
  VM_ADD_BUILTIN("rot", rot);
  VM_ADD_BUILTIN("depth", depth);

  // return stack manipulation
  VM_ADD_BUILTIN(">r", to_r);
  VM_ADD_BUILTIN("r>", from_r);
}
