#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vm.h"

static const vm_cell_t NULL_CELL = {.as_code = NULL};

/**
 * Allocate memory (size in unit of cells)
 */
vm_cell_t *vm_alloc(vm_t *vm, vm_int size) {
  vm_cell_t *addr = vm->code_ptr;
  vm->code_ptr += size;
  return addr;
}

/**
 * Create a new word in the VM, but do not add it to the dictionary.
 * Normally the function vm_add_word should be called instead, except for
 * a few built-ins that are required for the interpreter.
 */
vm_word_t *vm_create_word(vm_t *vm, const char *name, vm_word_fn exec,
                          vm_cell_t data) {
  // vm_word_t *new_word = (vm_word_t *)malloc(sizeof(vm_word_t));
  vm_word_t *new_word =
      (vm_word_t *)vm_alloc(vm, sizeof(vm_word_t) / sizeof(vm_cell_t));
  new_word->name = (char *)malloc((strlen(name) + 1) * sizeof(char));
  strcpy(new_word->name, name);
  new_word->exec = exec;
  new_word->data = data;
  new_word->next = NULL;
  return new_word;
}

VM_BUILTIN(cells) {
  vm_int n1 = vm_pop(vm).as_int;
  vm_cell_t n2;
  n2.as_int = sizeof(vm_cell_t) * n1;
  vm_push(vm, n2);
}

VM_BUILTIN(comma) {
  printf("debug comma\n");
  printf(" code_ptr %p\n", vm->code_ptr);
  *(vm->code_ptr++) = vm_pop(vm);
}

VM_BUILTIN(create) {
  const char *name = vm_next_token(vm);
  vm_cell_t *word_start = vm->code_ptr;
  vm_cell_t *addr = NULL;

  /* compile the created word */
  VM_COMPILE_WORD_BY_NAME("_literal");
  addr = vm->code_ptr++; // reserve space for the address
  VM_COMPILE_WORD_BY_NAME("ret");

  vm_cell_t data;
  data.as_code = word_start;
  vm_add_word(vm, &vm->dictionary, name, &vm_builtin_call, data);

  // fill in the literal value
  addr->as_code = vm->code_ptr;
}

/**
 * (x addr -- )
 */
VM_BUILTIN(store) {
  vm_cell_t *addr = vm_pop(vm).as_code;
  vm_cell_t val = vm_pop(vm);
  /* #ifdef VM_CHECK_BOUNDS */
  /*   if (addr < vm->data_segment || addr >= vm->data_segment + vm->dp) { */
  /*     printf("ERROR: invalid memory address %p\n", (void *)addr); */
  /*     return; */
  /*   } */
  /* #endif */
  *addr = val;
}

/**
 * ( addr -- x )
 */
VM_BUILTIN(retrieve) {
  vm_cell_t *addr = vm_pop(vm).as_code;
  /* #ifdef VM_CHECK_BOUNDS */
  /*   if (addr < vm->data_segment || addr >= vm->data_segment + vm->dp) { */
  /*     printf("ERROR: invalid memory address %p\n", (void *)addr); */
  /*     return; */
  /*   } */
  /* #endif */
  vm_push(vm, *addr);
}

VM_BUILTIN(literal) { vm_push(vm, *(vm->ip++)); }

void vm_add_data_builtins(vm_t *vm) {
  VM_ADD_BUILTIN("create", create);
  VM_ADD_BUILTIN(",", comma);
  VM_ADD_BUILTIN("cells", cells);
  VM_ADD_BUILTIN("!", store);
  VM_ADD_BUILTIN("@", retrieve);
  VM_ADD_BUILTIN("_literal", literal);
}
