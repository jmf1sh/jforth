#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vm.h"

static const vm_cell_t NULL_CELL = {.as_code = NULL};

#define VM_ARITHMETIC_BUILTIN(name, operation)                                 \
  VM_BUILTIN(name) {                                                           \
    vm_int a;                                                                  \
    vm_int b;                                                                  \
    vm_cell_t c;                                                               \
    b = vm_pop(vm).as_int;                                                     \
    a = vm_pop(vm).as_int;                                                     \
    c.as_int = operation;                                                      \
    vm_push(vm, c);                                                            \
  }

VM_ARITHMETIC_BUILTIN(add, a + b)
VM_ARITHMETIC_BUILTIN(sub, a - b)
VM_ARITHMETIC_BUILTIN(mul, a *b)
VM_ARITHMETIC_BUILTIN(div, a / b)

VM_ARITHMETIC_BUILTIN(or, (size_t)a | (size_t)b)
VM_ARITHMETIC_BUILTIN(and, (size_t)a &(size_t)b)
VM_ARITHMETIC_BUILTIN(eq, a == b)
VM_ARITHMETIC_BUILTIN(neq, a != b)
VM_ARITHMETIC_BUILTIN(leq, a <= b)
VM_ARITHMETIC_BUILTIN(geq, a >= b)
VM_ARITHMETIC_BUILTIN(lt, a < b)
VM_ARITHMETIC_BUILTIN(gt, a > b)

VM_BUILTIN(1plus) {
  vm_cell_t a = vm_pop(vm);
  ++a.as_int;
  vm_push(vm, a);
}

void vm_add_arithmetic_builtins(vm_t *vm) {
  VM_ADD_BUILTIN("1+", 1plus);
  VM_ADD_BUILTIN("+", add);
  VM_ADD_BUILTIN("-", sub);
  VM_ADD_BUILTIN("*", mul);
  VM_ADD_BUILTIN("/", div);
  VM_ADD_BUILTIN("or", or);
  VM_ADD_BUILTIN("and", and);
  VM_ADD_BUILTIN("=", eq);
  VM_ADD_BUILTIN("!=", neq);
  VM_ADD_BUILTIN("<", lt);
  VM_ADD_BUILTIN(">", gt);
  VM_ADD_BUILTIN("<=", leq);
  VM_ADD_BUILTIN(">=", geq);
}
