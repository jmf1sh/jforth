$env:vspath = 'C:\program files (x86)\microsoft visual studio'
$env:cmakepath = "$env:vspath\2019\buildtools\Common7\IDE\CommonExtensions\Microsoft\CMake\CMake\bin"

echo vspath = "$env:vspath"
echo cmakepath = "$env:cmakepath"

function bailout {
    if ($LASTEXITCODE -ne 0) {
	echo "last command failed; exiting"
	exit 1
    }
}

& "$env:vspath\2019\buildtools\Common7\Tools\vsdevcmd\ext\vcvars.bat" x64

mkdir build
cd build
#& "$env:cmakepath\cmake.exe" -G 'Visual Studio 15 2017 Win64' -DBUILD_MSVC=ON ..
& "$env:cmakepath\cmake.exe" -G 'Visual Studio 16 2019' -DBUILD_MSVC=ON ..
bailout

& "$env:cmakepath\cmake.exe" --build . --config Debug
bailout

& "$env:cmakepath\ctest.exe" --output-on-failure -C Debug
bailout

exit $LASTEXITCODE
