#! /bin/bash

set -eu

mkdir -p /opt/watcom
wget https://github.com/open-watcom/open-watcom-v2/releases/download/Current-build/ow-snapshot.tar.gz
tar xzf ow-snapshot.tar.gz -C /opt/watcom
# following commands taken from aur pkgbuild
chmod -R 755 /opt/watcom/{h,lh}
ln -s /opt/watcom/binl/wlib /opt/watcom/binl/ar
ln -s /usr/bin/true /opt/watcom/binl/ranlib
mkdir build && cd build
source ../watcom.sh
cmake .. -DBUILD_WATCOM=ON -DCMAKE_C_COMPILER=$CC -DCMAKE_CXX_COMPILER=$CC -G "Watcom WMake"
cmake --build .
ctest
