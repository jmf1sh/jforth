jforth
==================
[![pipeline status](https://gitlab.com/jmf1sh/jforth/badges/master/pipeline.svg)](https://gitlab.com/jmf1sh/jforth/-/commits/master) [![coverage report](https://gitlab.com/jmf1sh/jforth/badges/master/coverage.svg)](https://gitlab.com/jmf1sh/jforth/-/commits/master)

Minimalist(ish) Forth interpreter in pure C. The implementation depends on only a handful of stdlib routines, and the goal is to eventually make even these optional with compile-time flags. We don't make any assumptions about integer width and instead rely on stdint. Long term, it would be nice to be able to cross-compile to classic architectures (e.g. x86 16-bit real mode).

Build instructions
------------------
Build and test:
```bash
mkdir build && cd build
cmake .. && make && ctest
```
Clang-tidy and -format (not compatible with coverage, see below):
```bash
cmake . -DCOVERAGE=OFF && make clang-format && make clang-tidy
```

Code coverage (requires gcovr; only tested with gcc)
```bash
cmake . -DCOVERAGE=ON && make && ctest && make coverage
```

Profile a script:
```bash
cmake . -DPROFILE=ON && make
cat script.fs | ./jfrepl
gprof ./jfrepl
```

There are a few essential C tests than ensure the interpreter can bootstrap itself. All other tests are implemented in Forth and run through the interpreter. Code coverage report includes the code hit by running Forth tests in the interpreter.


Cross-compiling for DOS
------------------------
You can cross-compile to 16- or 32-bit DOS using [OpenWatcom](https://github.com/open-watcom/open-watcom-v2). Instructions
```bash
mkdir build && cd build
source ../watcom.sh
cmake -DWATCOM=ON -DBUILD_DOS32=ON -G "Watcom WMake" ..
cmake --build .
cp jfrepl jfrepl.exe
dosbox jfrepl.exe
```
You will need a copy of dos4gw to run in 32-bit extended mode. Currently we can build both 16- and 32-bit version without error. The 32-bit build appears to work correctly; however the 16-bit build seems to be broken at the moment.


VM internals
------------
At program start, the top of the stack contains the value 0. At exit, the REPL will use the value stored on the top of the stack as the exit code. This allows for unit testing, e.g.
```forth
: expect-eq != if 1 bye then ;
2 3 expect-eq \ process will exit with exit code 1
```
There is a non-standard word `.disassemble` that will examine the contents of the code/data segment, e.g.
```forth
: expect-eq != if 1 bye then ;
: add2 2 + ;
expect-eq:
    0x7f53555565f8    !=
    0x7f5355556600    0branch
    0x7f5355556608    0x7f5355556628
    0x7f5355556610    _literal
    0x7f5355556618    0x1
    0x7f5355556620    bye
    0x7f5355556628    ret
    0x7f5355556630    0x5563f1337070
    0x7f5355556638    0x5563f0fcc56c
    0x7f5355556640    expect-eq
    0x7f5355556648    0x7f5355556650
add2:
    0x7f5355556650    _literal
    0x7f5355556658    0x2
    0x7f5355556660    +
    0x7f5355556668    ret
```

Embedded VM
-----------
The VM was designed to be able to be embedded in arbitrary C/C++ applications. The VM is not thread safe but should be reentrant.
```c

/* register arbitrary native callbacks */
VM_BUILTIN(my_native_function) { /* do stuff with vm... */ }

/* instantiate and run VM */
vm_t vm;
vm_initialize(&vm);
VM_ADD_BUILTIN("my-native-function", my_native_function);

while (!vm.quit) {
  vm.current_word = (vm.ip++)->as_word;
  vm.current_word->exec(&vm);
}
```
Native C/C++ functions can be a mix of native and Forth code, e.g.
```c
VM_BUILTIN(blah) {
  vm_cell_t val;
  vm_int = vm_pop(vm).as_int; /* get argument from stack */
  int x2 = x*x; /* native code */
  int x3 = x*x*; /* native code */
  val.as_int = x*x;
  vm_push(vm, val); /* push value onto Forth stack */
  val.as_int = x*x*x;
  vm_push(vm, val); /* push value onto Forth stack */
  /* call a Forth function */
  vm_find_word(vm.dictionary, "add")->exec();
}

/* after VM is initialized, register function */
VM_ADD_BUILTIN("blah", blah);
```

Resources
----------
* https://www.forth.com/starting-forth/
* http://thinking-forth.sourceforge.net/
* https://forth-standard.org
* https://www.complang.tuwien.ac.at/forth/gforth/Docs-html/
* http://www.w3group.de/forth_course.html
* https://github.com/gerryjackson/forth2012-test-suite
* https://dillinger.io/
