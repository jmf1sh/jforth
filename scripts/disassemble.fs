
: fibonacci
  dup 0 <= if drop 0
	   else dup 1 = if drop 1

           else 1 - 0 1 rot 0
                do
                  dup rot + loop
                swap drop
           then then ;

: loop-fibonacci 0 do 100 fibonacci drop loop ;

.disassemble
bye
