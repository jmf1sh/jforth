# use `source watcom.sh` to set environment variables
# Then run
# cmake -DCMAKE_C_COMPILER=$CC -DCMAKE_CXX_COMPILER=$CC -DBUILD_WATCOM=ON <source-dir>
# cmake --build . && ctest

# to cross-compile for dos, use options BUILD_DOS16 or BUILD_DOS32 and rebuild
# cp jfrepl jfrepl.exe
# dosbox jfrepl.exe

export WATCOM=/opt/watcom
export PATH=$WATCOM/binl:$PATH
export EDPATH=$WATCOM/eddat
export WIPFC=$WATCOM/wipfc
export INCLUDE=$WATCOM/h
export CC=/opt/watcom/binl/owcc
export AR=/usr/bin/ar
#export CC=/opt/watcom/binl64/wcl386
