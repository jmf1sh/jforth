
find_program(GCOVR_COMMAND gcovr)

if(GCOVR_COMMAND)
  set(CMAKE_GCOVR_FOUND TRUE)
else()
  message(ERROR "could not locate gcovr executable, cannot add code coverage custom target!")
endif()

if(CMAKE_C_COMPILER_ID STREQUAL "GNU")
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -coverage")
else()
  message(ERROR "C compiler is not GNU; don't know how to set code coverage flags!")
endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -coverage")
else()
  message(ERROR "CXX compiler is not GNU; don't know how to set code coverage flags!")
endif()

# function to add a coverage target
# it will scan the working directory for coverage info, ignoring the directories in ignore_directories
function(add_coverage_target targetname ignore_directories)

  if (NOT CMAKE_GCOVR_FOUND)
    message(FATAL_ERROR "gcovr not found! aborting")
  endif()

  add_custom_target(${targetname}
    COMMAND ${GCOVR_COMMAND} -s
    --html-details coverage/${targetname}.html
    -r ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMAKE_CURRENT_BINARY_DIR}
    -e ${ignore_directories}
  )

endfunction()
